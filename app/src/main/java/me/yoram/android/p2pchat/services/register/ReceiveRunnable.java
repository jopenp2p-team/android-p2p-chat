/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.android.p2pchat.services.register;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 06/07/18
 */
public class ReceiveRunnable implements Runnable {
    private LocalBroadcastManager localBroadcastManager;
    private DatagramSocket socket;

    public ReceiveRunnable(DatagramSocket socket, LocalBroadcastManager localBroadcastManager) {
        super();

        this.socket = socket;
        this.localBroadcastManager = localBroadcastManager;
    }

    @Override
    public void run() {
        byte[] buf = new byte[1024];
        while (true) {
            try {
                DatagramPacket p = new DatagramPacket(buf, buf.length);
                socket.receive(p);

                String msg = new String(p.getData(), p.getOffset(), p.getLength()).replace("\n", " ");
                Intent intent = new Intent(RegistrationService.MESSAGE_INTENT);
                intent.putExtra(RegistrationService.MESSAGE_DATA, msg);

                localBroadcastManager.sendBroadcast(intent);

                Log.i("P2P", "<== " + p.getAddress() + "/" + p.getPort() + " <== " + msg);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }
}
