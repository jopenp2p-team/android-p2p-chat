/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.android.p2pchat.services;

import android.util.Log;

import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 12/07/18
 */
public class Config {
    private static InetAddress cServerIp;

    public static String getUsername() {
        return "Android client";
    }

    public static InetAddress getcServerIp() {
        if (cServerIp == null) {
            try {
                cServerIp = InetAddress.getByName("yoram.me");
            } catch (Throwable t) {
                Log.e("P2P", t.getMessage(), t);
                cServerIp = null;
            }
        }

        return cServerIp;
    }

    public static int getcPort() {
        return 5666;
    }
}
