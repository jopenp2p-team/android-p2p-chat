/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.android.p2pchat.services.register;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import me.yoram.android.p2pchat.services.Config;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 12/07/18
 */
public class RegistrationService extends Service {
    public class RegistrationServiceBinder extends Binder {
        public RegistrationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return RegistrationService.this;
        }
    }


    public static String MESSAGE_INTENT = RegistrationService.class.getName() + ".INTENT";
    public static String MESSAGE_DATA = MESSAGE_INTENT + ".DATA";

    private DatagramSocket serverSocket;
    private Timer timer;
    private Context ctx;
    private ReceiveRunnable receiveRunnable;
    private Thread receiveThread;
    private RegistrationServiceBinder binder = new RegistrationServiceBinder();

    @Override
    public IBinder onBind(Intent arg0) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        startService();
    }

    public void startService() {
        try {
            serverSocket = new DatagramSocket(19876);
        } catch (Throwable t) {
            Log.e("P2P", t.getMessage(), t);
            throw new RuntimeException(t.getMessage(), t);
        }

        timer = new Timer();
        timer.schedule(new MainTask(serverSocket), 0, 5000);

        receiveRunnable = new ReceiveRunnable(serverSocket, LocalBroadcastManager.getInstance(ctx));
        receiveThread = new Thread(receiveRunnable);
        receiveThread.start();
    }

    public void sendMessage(final String dest, final String msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String[] parts = dest.split("/");

                try {
                    DatagramPacket p = new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(parts[0]), Integer.parseInt(parts[1]));
                    serverSocket.send(p);
                } catch (Throwable t) {
                    Log.e("P2P", t.getMessage(), t);
                    throw new RuntimeException(t.getMessage(), t);
                }
            }
        }).start();
    }

    private class MainTask extends TimerTask
    {
        private final DatagramSocket socket;

        public MainTask(DatagramSocket socket) {
            super();

            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                if (isReady()) {
                    final String user = Config.getUsername();
                    final String msg = "register " + (user != null && !user.trim().isEmpty() ? user.trim() : "");
                    final DatagramPacket sendPacket = new DatagramPacket(
                            msg.getBytes(), msg.length(), Config.getcServerIp(), Config.getcPort());
                    socket.send(sendPacket);

                    Thread.sleep(5000);
                }
            } catch (Throwable t) {
                // DO NOTHINGseyt
            }
        }

        private boolean isReady() {
            return Config.getcServerIp() != null && Config.getcPort() > 0;
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        timer.cancel();
        receiveThread.interrupt();
        serverSocket.close();
        Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
    }
}
